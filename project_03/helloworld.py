"""My fist helloworld module"""


def print_me(text):
    """method prints out the given text"""
    print(text)


if __name__ == "__main__":
    print_me("Hello World")
