from project_03.math_operations import multiplication

def test_multiplication():
    assert 24 == multiplication(4, 6)
    assert 4 == multiplication(2, 2)
